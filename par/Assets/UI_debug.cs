﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_debug : MonoBehaviour {



    private CharacterController playerController;
    private Text UiText;
    



    // Use this for initialization
    void Start () {
        playerController = GameObject.Find("PlayerFPSController").GetComponent<CharacterController>();
        UiText = GetComponent<Text>();
       



    }
	
	// Update is called once per frame
	void Update () {
        UiText.text = "combined speed" + '\n'
            + playerController.velocity.magnitude.ToString("F4") + '\n'
            + "right, up, forward" + '\n' 
            + playerController.transform.InverseTransformDirection(playerController.velocity);
        
		
	}
}
