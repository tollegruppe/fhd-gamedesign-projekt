﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;


namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]


    public class PlayerFPSController : MonoBehaviour
    {

        [Tooltip("forwards/backwards speed")]
        public float speedZ = 10f;

        [Tooltip("strafe speed")]
        public float speedX = 10f;

        [Tooltip("multiplier for air control speed")]
        public float airControlMultiplier = 0.7f;

        public float jumpForce = 10f;

        public float gravity = 40.0f;


        public float timeRunStart = 0f; //time when the player starts running
        public float runningTime; //time the character currently has runned without stopping
        public float maxRunningTime = 10f; //time the character has to run before reaching maximum speed
        public float restIncrement = 0f;

        [Tooltip("How quickly the player slows down if no input is happening")]
        public float slowdownMult = 0.05f;
        [Tooltip("How quickly the player slows down if he is sliding")]
        public float slideSlowdownMult = 0.02f;

        private float tCrouch = 0f;
        private float crouchHeight = 0.5f;
        private float standHeight = 1.7f;

        [Tooltip("Maximum Speed while Crouching")]
        public float speedCrouchMaxX = 6f;
        public float speedCrouchMaxZ = 6f;

        public Camera cam;
        public MouseLook mouseLook = new MouseLook();

        private CharacterController controller;
        public AnimationCurve accelerationCurve;


        //Finite State Machine vars
        public States state_now;
        public enum States { StandingIdle, Running, CrouchedIdle, CrouchedMoving, Falling, JumpingUp, Sliding, Braking, Vaulting, Wallclimbing, Wallrunning }; //add new states here

        //desired movement direction
        public Vector3 moveDirection;
        private Vector3 RayBlock;

        //player inputs
        private Vector2 inputHorizontal;
        private bool inputJump;
        private bool inputUp;
        private bool inputCrouch;
        public Vector3 previousMove = new Vector3(0, 0, 0);

        //vault objects
        private Vector3 RayVault;
        public float vaultObjectDistance;
        public bool vaultReady;
        public RaycastHit hitV;
        public GameObject vaultObject;

        //wallclimb objects
        private Vector3 RayWallclimb;
        public bool wallclimbReady;
        public RaycastHit hitWC;
        public int wallJumpEnd = 0;

        //wallrun objects
        private Vector3 RayWallrun;
        public bool wallrunReady;
        public RaycastHit hitWR;



        void Start()
        {

            mouseLook.Init(transform, cam.transform);

            controller = GetComponent<CharacterController>();

            state_now = States.StandingIdle;

            


        }


        //Rotates Camera based on mouse input
        private void RotateView()
        {
            mouseLook.LookRotation(transform, cam.transform);
        }


        //Gets player input
        private void GetPlayerInput()
        {
            inputHorizontal = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical"));
            inputJump = CrossPlatformInputManager.GetButtonDown("Jump");
            inputUp = CrossPlatformInputManager.GetButton("Jump");
            inputCrouch = CrossPlatformInputManager.GetButton("Crouch");
        }


        //Checks if player is grounded
        private bool GroundCheck()
        {
            return controller.isGrounded;
        }



        //Update
        void Update()
        {


            RotateView();
            GetPlayerInput();
 

            //print("State" + state_now.ToString() +moveDirection);

        }



        //FixedUpdate
        private void FixedUpdate()
        {

            // Apply gravity


            moveDirection.y -= gravity * Time.fixedDeltaTime;


            //move player based on moveDirection
            controller.Move(moveDirection * Time.fixedDeltaTime);

            //FSM calls function of current state
            switch (state_now)
            {
                case States.StandingIdle: StandingIdle(); break;

                case States.Running: Running(); break;

                case States.CrouchedIdle: CrouchedIdle(); break;

                case States.CrouchedMoving: CrouchedMoving(); break;

                case States.Falling: Falling(); break;

                case States.JumpingUp: JumpingUp(); break;

                case States.Sliding: Sliding(); break;

                case States.Braking: Braking(); break;

                case States.Vaulting: Vaulting(); break;

                case States.Wallclimbing: Wallclimbing(); break;

                case States.Wallrunning: Wallrunning(); break;

                default: StandingIdle(); break;


            }


            //Update previous Movement vector after all the movement is applied so it remains accurate for the next Frame
            previousMove = moveDirection;

        }



        //Call this function to switch states
        /// <summary>
        /// Switches FSM to a new state 
        /// </summary>
        /// <param name="newState">the new state</param>
        private void SwitchState(States newState)
        {
            state_now = newState;
        }



        //  FSM STATE FUNCTIONS


        private void StandingIdle()
        {
            speedZ = 1f;
            runningTime = 0f;
            restIncrement = 0f;
            moveDirection = (transform.TransformDirection(Vector3.zero));


            //Transition Running
            if (inputHorizontal != Vector2.zero)
            {
                timeRunStart = Time.time;
                SwitchState(States.Running);
                moveDirection.y = 0f;

            }


            //Transition JumpingUp
            if (inputJump) SwitchState(States.JumpingUp);

            //Transition Falling
            if (!GroundCheck()) SwitchState(States.Falling);

            //Transition CrouchIdle
            if (inputCrouch == true && inputHorizontal == Vector2.zero) SwitchState(States.CrouchedIdle);

            //Transition Crouchmoving
            if (inputCrouch == true && inputHorizontal != Vector2.zero) SwitchState(States.CrouchedMoving);



        }
        private void Braking()
        {

            //Slow down because no movement input
            timeRunStart = 0f;          
            moveDirection.x *= 1f - slowdownMult;
            moveDirection.z *= 1f - slowdownMult;
            moveDirection.y = 0;
            speedZ *= 1f - slowdownMult;
            vaultReady = false;
            wallclimbReady = false;
            if (runningTime < maxRunningTime)
                restIncrement = runningTime *= 1f - slowdownMult;
            else
                restIncrement = maxRunningTime *= 1f - slowdownMult;

            //Transition Running
            if (inputHorizontal != Vector2.zero)
            {
                timeRunStart = Time.time;
                SwitchState(States.Running);

            }

            //Transition StandingIdle
            if (inputHorizontal == Vector2.zero && controller.velocity.x <= Mathf.Epsilon && controller.velocity.z <= Mathf.Epsilon) SwitchState(States.StandingIdle);

            //Transition JumpingUp
            if (inputJump) SwitchState(States.JumpingUp);

            //TransitionCrouchIdle
            if (inputCrouch == true && inputHorizontal == Vector2.zero && speedZ < 10) SwitchState(States.CrouchedIdle);

            //Transition Crouchmoving
            if (inputCrouch == true && inputHorizontal != Vector2.zero && speedZ < 10) SwitchState(States.CrouchedMoving);



        }

        private void Running()
        {

            RayBlock = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            runningTime = restIncrement + (Time.time - timeRunStart);
            gravity = 40f;

            //only apply moveDirection if input is happening, otherwise speed will be instantly set to zero and braking/slowdown does not happen
            if (inputHorizontal != Vector2.zero)
            {
                if (Physics.Raycast(RayBlock, transform.TransformDirection(Vector3.forward), 0.6f))
                {
                    speedZ = 1f;
                    timeRunStart = Time.time;
                }
                else
                    speedZ = accelerationCurve.Evaluate(runningTime);
                moveDirection = (transform.TransformDirection(new Vector3(inputHorizontal.x * speedX, 0, inputHorizontal.y * speedZ)));
            }
           

            


                       
            //check for vaulting. wallrunning, wallclimbing

            RayVault = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            RayWallclimb = new Vector3(transform.position.x, transform.position.y+1f, transform.position.z);
            RayWallrun = new Vector3(transform.position.x, transform.position.y+1f, transform.position.z);
            Debug.DrawRay(RayVault, transform.rotation * Vector3.forward * 3f, Color.red);
            Debug.DrawRay(RayWallclimb, transform.rotation * Vector3.forward * 3f, Color.red);
            Debug.DrawRay(RayWallrun, transform.rotation * Vector3.right * 1.5f, Color.red);
            Debug.DrawRay(RayWallrun, transform.rotation * Vector3.left * 1.5f, Color.red);

            if (Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), out hitV, 3f))
                vaultReady = true;
                vaultObjectDistance = hitV.distance;
            if (!Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), out hitV, 3f))
                vaultReady = false;

            if (Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), out hitWC, 3f ) && Mathf.Round(hitWC.distance) == Mathf.Round(hitV.distance) && !Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.right), 1.5f) && !Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.left), 1.5f))
                wallclimbReady = true;
            if (!Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), out hitWC, 3f))
                wallclimbReady = false;

            if (Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.right), out hitWR, 1f) || Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.left), out hitWR, 1f))
                wallrunReady = true;
            if (!Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.right), 1f) && !Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.left), 1f))
                wallrunReady = false;


            //Transition JumpingUp
            if (inputJump && !vaultReady) SwitchState(States.JumpingUp);

            //Transition Falling
            if (!GroundCheck()) SwitchState(States.Falling);

            //Transition Breaking
            if (inputHorizontal == Vector2.zero && GroundCheck())
            {
                SwitchState(States.Braking);

            }
            
            //Transition CrouchIdle
            if (inputCrouch == true && inputHorizontal == Vector2.zero && speedZ <= 12f) SwitchState(States.CrouchedIdle);

            //Transition Crouchmoving
            if (inputCrouch == true && inputHorizontal != Vector2.zero && speedZ <= 12f) SwitchState(States.CrouchedMoving);

            //Transition Sliding
            if (inputCrouch == true && speedZ >= 12f) SwitchState(States.Sliding);

            //Transition Vaulting
            if (inputUp && GroundCheck()  && vaultReady && !wallclimbReady)
            {
                
                speedZ = 0f;
                moveDirection.z = 0f;
                moveDirection.x = 0f;
                print(vaultObjectDistance);
                Physics.IgnoreCollision(controller, hitV.collider, true);
                vaultObject=hitV.collider.gameObject;
                StartCoroutine(VaultAnimation());
                SwitchState(States.Vaulting);
                
            }
            
            //Transition Wallclimbing
            if (inputUp && vaultReady && wallclimbReady)
            {
                StartCoroutine(WallclimbAnimation());
                vaultObject = hitWC.collider.gameObject;
                SwitchState(States.Wallclimbing);
            }

            //Transition Wallrunning
            if (inputUp && wallrunReady && !wallclimbReady)
            {
                StartCoroutine(WallRunAnimation());
                SwitchState(States.Wallrunning);
            }



        }

        private void Sliding()
        {

            timeRunStart = 0f;
            moveDirection.x *= 1f - slideSlowdownMult;
            moveDirection.z *= 1f - slideSlowdownMult;
            speedZ *= 1f - slideSlowdownMult;
            if (runningTime < maxRunningTime)
                restIncrement = runningTime *= 1f - slideSlowdownMult;
            else
                restIncrement = maxRunningTime *= 1f - slideSlowdownMult;

            controller.height = 1.2f;

            //Transition CrouchIdle
            if (inputCrouch == true && inputHorizontal == Vector2.zero && speedZ <= 4f) SwitchState(States.CrouchedIdle);

            //Transition Crouchmoving
            if (inputCrouch == true && inputHorizontal != Vector2.zero && speedZ <= 4f) SwitchState(States.CrouchedMoving);

            // Transition Running
            if (inputCrouch == false && inputHorizontal != Vector2.zero)
            {
                timeRunStart = Time.time;
                SwitchState(States.Running);
            }

            //Transition Falling
            if (!GroundCheck()) SwitchState(States.Falling);

        }

        private void CrouchedIdle()
        {
            tCrouch += Time.deltaTime * 2f;
            controller.height = Mathf.Lerp(controller.height, crouchHeight, tCrouch);


            //Slow down because no movement input
            moveDirection.x *= 1f - slowdownMult;
            moveDirection.z *= 1f - slowdownMult;

            //Transition JumpingUp
            if (inputJump) SwitchState(States.JumpingUp);

            //Transition Falling
            if (!GroundCheck()) SwitchState(States.Falling);

            //Transition StandingIdle
            if (inputCrouch == false && inputHorizontal == Vector2.zero && GroundCheck())
            {
                SwitchState(States.StandingIdle);
                tCrouch += Time.deltaTime * 2f;
                controller.height = Mathf.Lerp(controller.height, standHeight, tCrouch);
            }

            //Transition Crouchmoving
            if (inputCrouch == true && inputHorizontal != Vector2.zero) SwitchState(States.CrouchedMoving);

        }

        private void CrouchedMoving()
        {
            speedX = 4f;
            speedZ = 4f;
            tCrouch += Time.deltaTime * 2f;
            controller.height = Mathf.Lerp(controller.height, crouchHeight, tCrouch);
            moveDirection = (transform.TransformDirection(new Vector3(inputHorizontal.x * speedX, 0, inputHorizontal.y * speedZ)));
            restIncrement = 0.8f;


            //Transition StandingIdle
            if (inputCrouch == false && inputHorizontal == Vector2.zero && GroundCheck())
            {
                SwitchState(States.StandingIdle);
                tCrouch += Time.deltaTime * 2f;
                controller.height = Mathf.Lerp(controller.height, standHeight, tCrouch);

            }

            //Transition CrouchIdle
            if (inputCrouch == true && inputHorizontal == Vector2.zero) SwitchState(States.CrouchedIdle);

            //Transition Running
            if (inputCrouch == false && inputHorizontal != Vector2.zero)
            {
                tCrouch += Time.deltaTime * 2f;
                controller.height = Mathf.Lerp(controller.height, standHeight, tCrouch);
                timeRunStart = Time.time;
                SwitchState(States.Running);

            }

            //Transition JumpingUp
            if (inputJump) SwitchState(States.JumpingUp);


            //Transition Falling
            if (!GroundCheck()) SwitchState(States.Falling);


        }

        private void Falling()
        {
            //air control

            wallJumpEnd = 0;
            gravity = 40f;

            RayVault = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            RayWallclimb = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            Debug.DrawRay(RayVault, transform.rotation * Vector3.forward * 3f, Color.red);
            Debug.DrawRay(RayWallclimb, transform.rotation * Vector3.forward * 3f, Color.red);

            if (Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), out hitV, 3f))
                vaultReady = true;
            vaultObjectDistance = hitV.distance;
            if (!Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), out hitV, 3f))
                vaultReady = false;

            if (Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), out hitV, 3f) && Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), out hitWC, 3f) && Mathf.Round(hitWC.distance) == Mathf.Round(hitV.distance))
                wallclimbReady = true;
            if (!Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), out hitWC, 3f))
                wallclimbReady = false;


            if (inputHorizontal == Vector2.zero)
            {
                moveDirection.x *= 1f - slowdownMult;
                moveDirection.z *= 1f - slowdownMult;
                speedZ *= 1f - slowdownMult;
                if (runningTime < maxRunningTime)
                    restIncrement = runningTime *= 1f - slowdownMult;
                else
                    restIncrement = maxRunningTime *= 1f - slowdownMult;
            }
            else
            {
                if (runningTime < maxRunningTime)
                    restIncrement = runningTime *= 1f - slowdownMult;
                else
                    restIncrement = maxRunningTime *= 1f - slowdownMult;
                moveDirection = (transform.TransformDirection
                                            (

                                            new Vector3(inputHorizontal.x * speedX * airControlMultiplier,
                                            moveDirection.y,
                                            inputHorizontal.y * speedZ * airControlMultiplier

                                            )));

                if (inputUp && vaultReady && wallclimbReady)
                {
                    StartCoroutine(WallclimbAnimation());
                    vaultObject = hitWC.collider.gameObject;
                    SwitchState(States.Wallclimbing);
                }
            }

            //Transition Running

            if (GroundCheck() && inputHorizontal != Vector2.zero)
            {
                timeRunStart = Time.time;
                SwitchState(States.Running);
            }


            //Transition Breaking
            if (GroundCheck() && inputHorizontal == Vector2.zero && controller.velocity.x <= Mathf.Epsilon && controller.velocity.z <= Mathf.Epsilon)
            {
                SwitchState(States.Braking);
            }
        }

        private void JumpingUp()
        {

            moveDirection.y += jumpForce;
            SwitchState(States.Falling);

        }

        private void Vaulting()
        {

            
            vaultReady = false;
            
            

        }

        private void Wallclimbing()
        {
            RayVault = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            RayWallclimb = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
           

        }

        private void Wallrunning()
        {
            RayWallrun = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            RayVault = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            RayWallclimb = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);

        }






        IEnumerator VaultAnimation()
        {
           
                for (float m = 0; m <= 1f; m = m + 0.15f)
                {
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, hitV.point.x, m), Mathf.Lerp(transform.position.y, hitV.point.y + 1f, m), Mathf.Lerp(transform.position.z, hitV.point.z, m));

                
                if (m >=0.86f)
                {
                    
                   Physics.IgnoreCollision(controller, vaultObject.GetComponent<Collider>(), false);
                   SwitchState(States.Running);

                }

                    yield return null;
                }


                
            
        }

        IEnumerator WallclimbAnimation()
        {         
            gravity = 0f;
            moveDirection = Vector3.zero;
            for (int m = 0; m <= 45; m++)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, hitV.point.y + 2f, m*0.01f), transform.position.z);
                print(m);

                if (!Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), 1f))
                    {
                    vaultReady = false;
                    Physics.IgnoreCollision(controller, hitWC.collider, true);
                    
                    m = 45;
                    wallJumpEnd = 20;
                    
                }
                yield return null;

            }
            if (!Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), 1f) && wallJumpEnd < 20)
            {
                Physics.IgnoreCollision(controller, hitWC.collider, true);
                wallJumpEnd = 60;
            }
            else if (wallJumpEnd < 20)
                wallJumpEnd = 0;

            StartCoroutine(WallclimbAnimationEnd());

        }

        IEnumerator WallclimbAnimationEnd()
        {
            int mult = 1;
            if (wallJumpEnd == 60)
                mult = 3;
            Vector3 positionCache = transform.position;
            Vector3 localZ = transform.TransformDirection(new Vector3(0, 0, 1f));

            for (int m = 0; m <= 60; m++)
            {
                
                transform.position = new Vector3(Mathf.Lerp(positionCache.x, positionCache.x + localZ.x, m * 0.05f/mult), Mathf.Lerp(positionCache.y, hitV.point.y+localZ.y + 3f+mult/2.5f, m * 0.05f/mult),Mathf.Lerp(positionCache.z, positionCache.z+localZ.z, m * 0.05f/mult));
                if (m >= wallJumpEnd)
                    m = 60;
                yield return null;
            }
            Physics.IgnoreCollision(controller, vaultObject.GetComponent<Collider>(), false);
            SwitchState(States.Falling);
        }
       
       IEnumerator WallRunAnimation()
        {
            Vector3 positionChache = transform.position;
            Vector3 localZ = transform.TransformDirection(new Vector3 (0,0,10));       
            localZ=Vector3.ProjectOnPlane(localZ,hitWR.normal);
            print(hitWR.normal);
            

            for (int m = 0; m <= 100; m++)
            {
               
                transform.position = new Vector3(Mathf.Lerp(positionChache.x, positionChache.x + localZ.x, m*0.01f), Mathf.Lerp(positionChache.y, positionChache.y+1, m*0.01f), Mathf.Lerp(positionChache.z, positionChache.z+localZ.z, m*0.01f));
                if ((!Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.right), 1f) && !Physics.Raycast(RayWallrun, transform.TransformDirection(Vector3.left), 1f)) || Physics.Raycast(RayVault, transform.TransformDirection(Vector3.forward), 0.6f) || Physics.Raycast(RayWallclimb, transform.TransformDirection(Vector3.forward), 0.6f))
                    m = 100;
                    yield return null;
            }
            SwitchState(States.Falling);
        }
    }

}

