using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (Rigidbody))]
    [RequireComponent(typeof (CapsuleCollider))]
    public class RigidbodyFirstPersonController : MonoBehaviour
    {
       


        [Serializable]
        public class AdvancedSettings
        {
            public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
            public float stickToGroundHelperDistance = 0.5f; // stops the character
            public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
            
        }


        public float ForwardSpeed = 8.0f;   // Speed when walking forward
        public float BackwardSpeed = 4.0f;  // Speed when walking backwards
        public float StrafeSpeed = 4.0f;    // Speed when walking sideways
        

        public KeyCode RunKey = KeyCode.LeftShift;
        public float RunMultiplier = 2.0f;   // Speed multiplier for sprinting

        [Tooltip("Speed for Air Control, uses same scale as ground movement")]
        public float AirControlForce = 30f;  // Speed for air control

        public float JumpForce = 3f;
        


        [Tooltip("How quickly you stop strafing when on ground and no movement key is pressed")]
        public float SlowdownRateX = 1f;
        [Tooltip("How quickly you stop moving when on ground and no movement key is pressed")]
        public float SlowdownRateZ = 1f;

        [Tooltip("Multiplier for Drag, limits player speed and acceleration on horizontal movement")]
        public float DragMultiplierX = 0.025f;
        public float DragMultiplierZ = 0.025f;
        

        private bool m_Running;
        private float GravityMultiplier = 1f;

        public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));
        public bool Running
        {
            get { return m_Running; }
        }






        public Camera cam;
        public MouseLook mouseLook = new MouseLook();
        public AdvancedSettings advancedSettings = new AdvancedSettings();

        
        public float StandHeight = 1.7f;
        public float CrouchHeight = 0.5f;

        public float fallMultiplier = 2.5f;
        public float lowJumpMultiplier = 2f;


        [HideInInspector] public float CurrentTargetSpeed = 8f;

        private Rigidbody m_RigidBody;
        private CapsuleCollider m_Capsule;
        private float m_YRotation;
        private Vector3 m_GroundContactNormal;
        private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded, m_crouch, remain_crouched, wallJumpExecute, m_WallJump, m_sliding;








        private float t_slide = 0.0f;
        private float t_crouch = 0.0f;


        public Vector3 Velocity
        {
            get { return m_RigidBody.velocity; }
        }

        public bool Grounded
        {
            get { return m_IsGrounded; }
        }

        public bool Jumping
        {
            get { return m_Jumping; }
        }

      

        public void UpdateDesiredTargetSpeed(Vector2 input)
        {
            
            if (input == Vector2.zero)
            {

                CurrentTargetSpeed = m_RigidBody.velocity.sqrMagnitude;
                return;
            }
            if (input.x > 0 || input.x < 0)
            {
                //strafe
                CurrentTargetSpeed = StrafeSpeed;
            }
            if (input.y < 0)
            {
                //backwards
                CurrentTargetSpeed = BackwardSpeed;
            }
            if (input.y > 0)
            {
                //forwards
                //handled last as if strafing and moving forward at the same time forwards speed should take precedence
                CurrentTargetSpeed = ForwardSpeed;
            }
            

            if (Input.GetKey(RunKey))
            {
                if (!Jumping)
                    CurrentTargetSpeed *= RunMultiplier;
                m_Running = true;
            }
            else
            {
                m_Running = false;
            }
        }








        private void Start()
        {

      
            m_RigidBody = GetComponent<Rigidbody>();
            m_Capsule = GetComponent<CapsuleCollider>();
            mouseLook.Init (transform, cam.transform);
            m_Capsule.height = StandHeight;
          
            
        }

        private void Sliding()
        {
            if (m_crouch == true && !remain_crouched && m_RigidBody.velocity.magnitude >= 10f)
            {
                m_sliding=true;
                t_slide += Time.deltaTime * 0.1f;


                 DragMultiplierX = Mathf.Lerp(0.015f, 0.2f, t_slide);
                 DragMultiplierZ = Mathf.Lerp(0.015f, 0.2f, t_slide);
            }
          
        }


         private void Crouching()
        {

            if ((m_sliding == true && m_RigidBody.velocity.magnitude <= 5f) ||  (m_crouch == true && m_sliding == false && m_RigidBody.velocity.magnitude <= 10f || remain_crouched)) 
               
            {
                //shrinken height to crouch
                m_sliding = false;
                t_crouch += Time.deltaTime * 2f;
                m_Capsule.height = Mathf.Lerp(m_Capsule.height, CrouchHeight, t_crouch);
                ForwardSpeed = 0.5f;
                DragMultiplierX = 0.0355f;
                DragMultiplierZ = 0.0355f;


    }

            else if (m_crouch == false && !remain_crouched)
            {
                //reset scale.y to stand up
                t_crouch += Time.deltaTime * 2f;
                m_Capsule.height = Mathf.Lerp(m_Capsule.height, StandHeight, t_crouch);
                ForwardSpeed = 3f;
                DragMultiplierX = 0.025f;
                DragMultiplierZ = 0.025f;

            }
        }


        private void Update()
        {
            RotateView();
           


            if (CrossPlatformInputManager.GetButtonDown("Jump") && !m_Jump)
            {

                m_Jump = true;
               


            }

            if (CrossPlatformInputManager.GetButtonDown("Jump") && !m_IsGrounded)
            {
                m_WallJump = true;
            }



           

            if (Input.GetKeyDown(KeyCode.R))
            {
                ResetPosition();
            }

            if (CrossPlatformInputManager.GetButtonDown("Crouch") || CrossPlatformInputManager.GetButtonUp("Crouch"))
            {
                t_crouch = 0;
                t_slide = 0;

            }

            if (CrossPlatformInputManager.GetButton("Crouch"))
            {
                m_crouch = true;
                
            }
            else
            {
                m_crouch = false;
                
            }
            print(m_Jump);
            

        }
       


        private void FixedUpdate()
        {
            GroundCheck();
            
            remain_crouched = CheckCantStandUp();
            Crouching();
            Sliding();
            Movement();
            

            //apply gravity

            m_RigidBody.AddForce(Physics.gravity*GravityMultiplier, ForceMode.Acceleration);

            if (m_IsGrounded) GravityMultiplier = 1;







            //Apply drag on x (strafe) and z (forwards/backwards) to limit player top speed
            Vector3 vel = m_RigidBody.transform.InverseTransformDirection(m_RigidBody.velocity);
            vel.x *= 1 - DragMultiplierX; 
            vel.z *= 1 - DragMultiplierZ;
            m_RigidBody.velocity = m_RigidBody.transform.TransformDirection(vel);

            m_Jump = false;
            

        }

        private void Movement()
        {
            Vector2 input = GetInput();

            //if movement input
            if ((Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon))
            {
                // always move along the camera forward as it is the direction that it being aimed at
                Vector3 desiredMove = cam.transform.forward * input.y + cam.transform.right * input.x;

                desiredMove = Vector3.ProjectOnPlane(desiredMove, m_GroundContactNormal).normalized;

                desiredMove.x = desiredMove.x * CurrentTargetSpeed;
                desiredMove.z = desiredMove.z * CurrentTargetSpeed;
                desiredMove.y = desiredMove.y * CurrentTargetSpeed;

                //air movement
                if (!m_IsGrounded)
                {
                    m_RigidBody.AddForce(desiredMove * AirControlForce * 50f, ForceMode.Force);
                    if (m_WallJump)
                    {
                        Walljump();
                    }
                }

                //ground movement
                else
                
                {

                    m_RigidBody.AddForce(desiredMove * SlopeMultiplier() * 50f, ForceMode.Force);


                }
            }

            //if no movement input
            else
            {
                if (m_IsGrounded)
                {
                    // m_RigidBody.AddForce(-m_RigidBody.velocity *SlowdownRate, ForceMode.Force);

                }
            }

            //if on solid ground
            if (m_IsGrounded)
            {
                if (m_Jump)
                {
                    m_RigidBody.AddForce(new Vector3(0f, JumpForce, 0f), ForceMode.Impulse);
                    m_Jumping = true;
                }

                //Slowdown for strafing
                if (!m_Jump && Mathf.Abs(input.x) <= float.Epsilon)
                {
                    m_RigidBody.AddRelativeForce(new Vector3((-m_RigidBody.transform.InverseTransformDirection(m_RigidBody.velocity * SlowdownRateX).x), 0, 0));
                }

                //Slowdown for forwards/backwards
                if (!m_Jump && Mathf.Abs(input.y) <= float.Epsilon)
                {
                    m_RigidBody.AddRelativeForce(new Vector3(0, 0, (-m_RigidBody.transform.InverseTransformDirection(m_RigidBody.velocity * SlowdownRateZ).z)));
                }

                //Sleep rigidbody if no movement is happening at all
                if (!m_Jumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && m_RigidBody.velocity.magnitude < 1f)
                {
                    m_RigidBody.Sleep();
                }
            }

            //stick to ground if not jumping or falling
            else
            {

                if (m_PreviouslyGrounded && !m_Jumping)
                {
                    StickToGroundHelper();
                }

                //Control Gravity based on player velocity and jump input to make jump feel tighter

                //if falling down, set gravity to fallMultiplier
                if (m_RigidBody.velocity.y < 0)
                {
                    GravityMultiplier = fallMultiplier;
                }
                //The longer you press jump, the higher you jump
                else if (m_RigidBody.velocity.y > 0 && !CrossPlatformInputManager.GetButton("Jump"))
                {
                    GravityMultiplier = lowJumpMultiplier;
                }
                else
                {
                    GravityMultiplier = 1f;
                }



            }
        }
        
        private void Walljump()
        {
            if (wallJumpExecute)
            {
                m_RigidBody.AddForce(new Vector3(0f, JumpForce*2f - m_RigidBody.velocity.y, 0f), ForceMode.Impulse);
            }

            m_WallJump = false;
            wallJumpExecute = false;

        }

       
        private void OnCollisionStay(Collision collision)
        {
            
            var contact = collision.contacts[0];
                Debug.DrawRay(contact.point, contact.normal, Color.blue);

                if (!m_IsGrounded && contact.normal.y < 0.3f)
                {
                    Debug.DrawRay(contact.point, contact.normal *2f, Color.red);

                    if (m_WallJump)
                    {
                        wallJumpExecute = true;
                    }

                }

            
        }

        private void ResetPosition()
        {
            m_RigidBody.transform.position = new Vector3(2, 2, 9);
            
        }

        private bool CheckCantStandUp() //Returns true if the space above the player is not enough to stand up
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 1)) return true;
            else return false;
        }       

        private float SlopeMultiplier()
        {
            float angle = Vector3.Angle(m_GroundContactNormal, Vector3.up);
            return SlopeCurveModifier.Evaluate(angle);
        }


        private void StickToGroundHelper()
        {
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((m_Capsule.height/2f) - m_Capsule.radius) +
                                   advancedSettings.stickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
                {
                    m_RigidBody.velocity = Vector3.ProjectOnPlane(m_RigidBody.velocity, hitInfo.normal);
                }
            }
        }


        private Vector2 GetInput()
        {
            
            Vector2 input = new Vector2
                {
                    x = CrossPlatformInputManager.GetAxis("Horizontal"),
                    y = CrossPlatformInputManager.GetAxis("Vertical")
                };
			UpdateDesiredTargetSpeed(input);
            return input;
        }


        private void RotateView()
        {


           
            //avoids the mouse looking if the game is effectively paused
            if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

            // get the rotation before it's changed
            float oldYRotation = transform.eulerAngles.y;

            mouseLook.LookRotation (transform, cam.transform);

        }

        /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
        private void GroundCheck()
        {

            
            m_PreviouslyGrounded = m_IsGrounded;
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((m_Capsule.height/2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                m_IsGrounded = true;
                m_GroundContactNormal = hitInfo.normal;
            }
            else
            {
                m_IsGrounded = false;
                m_GroundContactNormal = Vector3.up;
            }
            if (!m_PreviouslyGrounded && m_IsGrounded && m_Jumping)
            {
                m_Jumping = false;
            }
        }
    }
}
